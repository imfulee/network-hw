# Network Homework Phase 2

- Compiler=gcc
- environment=Linux kernel >= 2.6

```makefile
all: client.o server.o
	gcc -g client.o -o client 
	gcc -g server.o -o server -lpthread

client.o: client.c
	gcc -g -c client.c

server.o: server.c
	gcc -g -c server.c -lpthread

clean:
	rm *.o output
```

## Usage of client binary

When using the client in terminal of a Linux environment

```bash
./client <IP address> <Port Number>
```

Then it would connect to the server, and the way to connect to the server would be to type in the commands, for ex. 

- Register a person named Bob

```bash
REGISTER#Bob
```

- Register a number for Bob with 1234

```bash
Bob#1234
```

- Listing all the users

```bash
List
```

- Exiting the program

```bash
Exit
```

That's the four commands that would be displayed.

## Usage of server binary

When using the server in terminal of a Linux environment 

```bash
./server <Port number>
```

Then it would start the server with multiple threads!
