#include <stdio.h> //printf
#include <string.h> //strcpy
#include <stdlib.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>	//hostent
#include <arpa/inet.h>
#include <pthread.h>
#include <stdbool.h>
#define maxUserNum 20

void *connection_handler(void *);
int userNum = 0, onlineNum = 0, accountBalance = 100;

struct People
{
	int port_num;
	char ip[16];
	char name[200];
};

struct People user[maxUserNum];

bool nameExist(char* targetName)
{
	/*
	if the name exists in the user array return true;
	else return false
	*/

	if (userNum == 0) return false;
	for (int i = 0 ; i < userNum ; i++)
	{
		if (strcmp(user[i].name, targetName) == 0)
		{
			return true;
		}
	}
	return false;
}

bool createUser(char targetName[])
{
	/*
	Creates a user
	If it is already full then it will return false
	*/

	if (userNum == maxUserNum - 1) return false;
	strcpy(user[userNum].name, targetName);
	user[userNum].port_num = 0;
	userNum += 1;
	return true;
}

bool assignNum(char targetName[], int num)
{
	/*
	Assign port num to user
	Else return false
	*/

	bool done = false;
	if (userNum == 0) return false;
	if(nameExist(targetName))
	{
		for (int i = 0 ; i < userNum ; i++)
		{
			char buffer[2000];
			strcpy(buffer, targetName);
			if (strcmp(user[i].name, buffer) == 0 && user[i].port_num != 0)
			{
				user[i].port_num = num;
				strcpy(user[i].ip, "127.0.0.53");
				onlineNum ++;
				done = true;
			}
		}
	}
	return done;
}

bool printAllUser(char *message)
{
	// Bit misleading title
	// The actual usage is to cat all the message
	char buffer[20*maxUserNum];
	
	for (int i = 0 ; i < userNum ; i++)
	{
		if (user[i].port_num != 0)
		{	
			strcpy(buffer, user[i].name);
			strcat(buffer, "#");
			strcat(buffer, user[i].ip);
			strcat(buffer, "#");
			char temp[5];
			sprintf(temp, "%d\n", user[i].port_num);
			strcat(buffer, temp);
		}
	}

	if (strcat(message, buffer) != NULL) return true;
	return false;
}

int main(int argc, char *argv[])
{
	int socket_desc, new_socket, c, *new_sock;
	struct sockaddr_in server, client;
	char message[2048];

	/*
	if (argc < 2)
	{
		printf("Too few arguments. Hint: port_num?");
		return 1;
	}
	*/
	
	//Create socket
	socket_desc = socket(AF_INET, SOCK_STREAM, 0);
	if (socket_desc == -1)
	{
		printf("Could not create socket");
		return 1;
	}
	
	//Prepare the sockaddr_in structure
	server.sin_family = AF_INET;
	server.sin_addr.s_addr = INADDR_ANY;
	// server.sin_port = htons(8888);
	server.sin_port = htons(atoi((char*)argv[1]));
	
	//Bind
	if( bind(socket_desc,(struct sockaddr *)&server, sizeof(server)) < 0)
	{
		puts("bind failed");
		return 1;
	}
	
	//Listen
	listen(socket_desc, 5);
	
	//Accept and incoming connection
	puts("Waiting for incoming connections...");
	c = sizeof(struct sockaddr_in);
	while((new_socket = accept(socket_desc, (struct sockaddr *)&client, (socklen_t*)&c)))
	{
		puts("Connection accepted");
		
		pthread_t sniffer_thread;
		new_sock = malloc(1);
		*new_sock = new_socket;
		
		if(pthread_create(&sniffer_thread, NULL, connection_handler, (void*) new_sock) < 0)
		{
			perror("could not create thread");
			return 1;
		}
		
		//Now join the thread, so that we dont terminate before the thread
		//pthread_join( sniffer_thread, NULL);
		puts("Handler assigned");
	}
	
	if (new_socket<0)
	{
		perror("accept failed");
		return 1;
	}
	
	return 0;
}

/*
 * This will handle connection for each client
 */
void *connection_handler(void *socket_desc)
{
	//Get the socket descriptor
	int sock = *(int*)socket_desc;
	int read_size;
	char message[2000], client_message[2000], targetName[200];
	
	strcpy(message, "Connection accepted\r\n");
	write(sock, message, strlen(message));
	memset(message, 0, sizeof(message));

	//Receive a message from client
	while((read_size = recv(sock, client_message, 2000, 0)) > 0)
	{
		if(strstr(client_message, "REGISTER") != NULL)
		{
			// For the case of registering someone
			puts(client_message);
			
			if(strstr(client_message, "REGISTER#") == NULL)
			{
				strcpy(message,"210 FAIL\r\n");
				write(sock, message, strlen(message));
				memset(client_message, 0, sizeof(client_message));
				continue;
			}
			
			// Process the command and make it usable
			char *name_ptr;
			name_ptr = strtok(client_message, "#");
			name_ptr = strtok(NULL, "#");
			strcpy(targetName, name_ptr);
			targetName[strlen(targetName)-1] = 0;
			
			// Conditions for the user to be created
			// And also create them
			if (nameExist(targetName))
			{
				// check if the name already exists
				strcpy(message, "210 FAIL\r\n");
				write(sock, message, strlen(message));
				memset(client_message, 0, sizeof(client_message));
				continue;	
			}
			else if (!createUser(targetName))
			{
				// create the name in the user database
				strcpy(message, "210 FAIL\r\n");
				write(sock, message, strlen(message));
				memset(client_message, 0, sizeof(client_message));
				continue;
			}

			strcpy(message,"100 OK\r\n");
			write(sock, message, strlen(message));

			// Cleaning up the message variables
			memset(client_message, 0, sizeof(client_message));
			memset(message, 0, sizeof(message));
			memset(targetName, 0, sizeof(targetName));
			name_ptr = NULL;
			continue;
		}
		else if(strcmp(client_message, "List\n") == 0)
		{
			puts(client_message);
			
			// Setup the header message
			char big_message[8192], buffer[2000];
			sprintf(big_message, "Account Balance: %d\n", accountBalance);
			sprintf(buffer, "number of online: %d\n", onlineNum);
			strcat(big_message, buffer);
			
			if (userNum > 0 && onlineNum > 0)
			{
				printAllUser(big_message);
			}else{
				strcpy(big_message, "220 AUTH_FAIL\n");
				write(sock, big_message, strlen(big_message));
				memset(big_message, 0, sizeof(big_message));
				memset(buffer, 0, sizeof(buffer));
				continue;
			}
			
			// Sending the data over
			write(sock, big_message, strlen(big_message));
			memset(client_message, 0, sizeof(client_message));
			memset(big_message, 0, sizeof(big_message));
			memset(buffer, 0, sizeof(buffer));
			continue;
		}
		else if(strcmp(client_message, "Exit\n") == 0)
		{
			// Sending the goodbye msg
			puts(client_message);
			onlineNum -= 1;
			strcpy(message, "Bye\r\n");
			write(sock, message, strlen(message));
			puts("Client disconnected");

			// Cleaning up the msg data
			memset(client_message, 0, sizeof(client_message));
			memset(message, 0, sizeof(message));
			close(sock);
			break;
		}
		else if(strchr(client_message, '#') != NULL)
		{
			puts(client_message);
			// This is the case for assigning port number
			char *name_ptr;
			name_ptr = strtok(client_message, "#");
			strcpy(targetName, name_ptr);

			if (nameExist(targetName))
			{
				char temp[200];
				name_ptr = strtok(NULL, "#");
				strcpy(temp, name_ptr);
				assignNum(targetName, atoi(temp));
			}
			else
			{
				strcpy(message, "220 AUTH_FAIL\n");
				write(sock, message, strlen(message));
				memset(message, 0, sizeof(message));
				continue;
			}

			// Printing the rest of the message
			char big_message[100000], buffer[2000];
			sprintf(big_message, "Account Balance: %d\n", accountBalance);
			sprintf(buffer, "number of online: %d\n", onlineNum);
			strcat(big_message, buffer);
			
			if (userNum > 0 && onlineNum > 0)
			{
				printAllUser(big_message);
			}
			
			// Sending the data over
			write(sock, big_message, strlen(big_message));
			memset(client_message, 0, sizeof(client_message));
			memset(targetName, 0, sizeof(targetName));
			memset(big_message, 0, sizeof(big_message));
			continue;
		}
		else // If there doesn't exists such a thing
		{
			strcpy(message, "Server: Understand your message, I do not\n");
			write(sock, message, strlen(message));
			memset(message, 0, sizeof(message));
			continue;
		}
	}
	
	if(read_size == 0)
	{
		puts("Client disconnected");
		fflush(stdout);
	}
	else if(read_size == -1)
	{
		perror("recv failed");
	}
		
	//Free the socket pointer
	free(socket_desc);
	
	return 0;
}