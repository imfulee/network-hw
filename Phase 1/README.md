# Network Homework Phase 1

- Compiler=gcc
- environment=Linux kernel >= 2.6

```makefile
all: client.o
	gcc client.o -o client
	rm client.o

client.o: client.c
	gcc -c client.c

clean:
	rm *.o output
```

## Usage of client binary

When using the client in terminal of a Linux environment

```bash
./client <IP address> <Port Number>
```

Then it would connect to the server, and the way to connect to the server would be to type in the commands, for ex. 

- Register a person named Bob

```bash
REGISTER#Bob
```

