#include <stdio.h>
#include <string.h>   //strlen
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>   //inet_addr

int main(int argc , char *argv[])
{
   int socket_desc;
   struct sockaddr_in server;
   char* message, server_reply[2000];

   //Check if the IP and port numbers are there
   if (argc < 3)
   {
      printf("Too few arguments!\n");
      return 1;
   }
   
   //Create socket
   socket_desc = socket(AF_INET , SOCK_STREAM , 0);
   if (socket_desc == -1)
   {
      printf("Could not create socket");
   }
      
   server.sin_addr.s_addr = inet_addr(argv[1]);
   server.sin_family = AF_INET;
   server.sin_port = htons(atoi(argv[2]));

   //Connect to remote server
   if (connect(socket_desc , (struct sockaddr *)&server , sizeof(server)) < 0)
   {
      puts("connect error");
      return 1;
   }
   else
   {
      recv(socket_desc, server_reply , 2000 , 0);
      printf("%s\n", server_reply);
   }

   while(1)
   {  
      //Scanning the message and send the data
      scanf("%s", message);
      strcat(message, "\n");
      memset(server_reply, 0, sizeof(server_reply));

      if(strcmp(message, "Exit\n") == 0)
      {
         send(socket_desc , message , strlen(message) , 0);
         recv(socket_desc, server_reply , 2000 , 0);
         puts(server_reply);
         break;
      }

      if(send(socket_desc , message , strlen(message) , 0) < 0)
      {
         puts("Send failed");
         return 1;
      }
      
      //Receive the reply from the server
      //sometimes the server sends more than one line (with \n)
      for (int i = 0 ; i < 3 ; i++)
      {   
         if (recv(socket_desc, server_reply , 2000, 0) < 0)
         {
            break;
            // puts("recv failed");
         }
         else
         {
            puts(server_reply);
            memset(server_reply, 0, sizeof(server_reply));
         }
      }
      //Reset the memory for the server reply
      memset(server_reply, 0, sizeof(server_reply));
   }

   //Close the socket
   close(socket_desc);
   
   return 0;
}
