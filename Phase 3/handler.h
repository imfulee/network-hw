#include "functions.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

void *connection_handler(void *socket_desc)
{
	//Get the socket descriptor
	int sock = *(int*)socket_desc;
	int read_size = 0;
	char message[100000], client_message[2000], name[2048], port[2048], recr[2048];
	
	strcpy(message, "Connection accepted\r\n");
	write(sock, message, strlen(message));
	memset(message, 0, sizeof(message));

	//Receive a message from client
	while((read_size = recv(sock, client_message, 2000, 0)) > 0)
	{
		memset(message, 0, sizeof(message));
		memset(name, 0, sizeof(name));
		memset(port, 0, sizeof(port));
		memset(recr, 0, sizeof(recr));
		
		if (strcmp(client_message, "1") == 0)
		{
			read_size = recv(sock, client_message, 2000, 0);
			printf("REGISTER#%s\n", client_message);
			
			if (name_exist(client_message))
			{
				strcpy(message, "210 FAIL\r\n");
			}
			else
			{
				strcpy(message, "100 OK\r\n");
				create_user(client_message);
			}
			write(sock, message, strlen(message));	
		}
		else if (strcmp(client_message, "2") == 0)
		{
			read_size = recv(sock, name, sizeof(name), 0);
			read_size = recv(sock, port, sizeof(port), 0);
			char buffer[5000];
			memset(buffer, 0, sizeof(buffer));
			sprintf(buffer, "%s#%s\n", name, port);
			printf("%s", buffer); // to comply with my teacher
			if (name_exist(name))
			{
				assign_num(name, port);
				cat_alluser(message);
			}
			else
			{
				strcpy(message, "220 AUTH FAIL\r\n");
			}
			write(sock, message, strlen(message));
		}
		else if (strcmp(client_message, "3") == 0)
		{
			puts("List");
			if (!cat_alluser(message))
			{
				strcpy(message, "220 AUTH FAIL\r\n");
			}
			write(sock, message, strlen(message));
		}
		else if (strcmp(client_message, "4") == 0)
		{
			read_size = recv(sock, name, sizeof(name), 0);
			read_size = recv(sock, recr, sizeof(recr), 0);
			read_size = recv(sock, port, sizeof(port), 0);
			printf("%s#%s#%s\n", name, port, recr);

			if (name_exist(name) && name_exist(recr))
			{
				if (transfer(name, recr, atoi(port)))
				{
					strcpy(message, "Transfer accepted\r\n");
					write(sock, message, strlen(message));
				}
				else
				{
					strcpy(message, "Not enough funds\r\n");
					write(sock, message, strlen(message));
				}
			}
			else
			{
				strcpy(message, "User not found\r\n");
				write(sock, message, strlen(message));
			}

		}
		else if (strcmp(client_message, "5") == 0)
		{
			puts("Exit");
			online_user -= 1;
			strcpy(message, "Bye\r\n");
			write(sock, message, strlen(message));
			puts("Client disconnected");
			close(sock);
			break;
		}
		else
		{
			strcpy(message, "Understand your message, server doesn't\n");
			write(sock, message, strlen(message));
			memset(message, 0, sizeof(message));
			continue;
		}
		memset(client_message, 0 , sizeof(client_message));
	}
	
	if(read_size == 0)
	{
		puts("Client disconnected");
		fflush(stdout);
	}
	else if(read_size == -1)
	{
		perror("recv failed");
	}
		
	//Free the socket pointer
	free(socket_desc);
	return 0;
}