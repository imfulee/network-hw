#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>   //strlen
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>   //inet_addr
#include <openssl/evp.h>
#include <openssl/ssl.h>
#include <openssl/rsa.h>
#include <openssl/x509.h>

int main(int argc , char *argv[])
{
   int socket_desc;
   struct sockaddr_in server;
   char message[2048], server_reply[2000];
   memset(server_reply, 0, sizeof(server_reply));

   printf("  ____ _ _            _   \n");
   printf(" / ___| (_) ___ _ __ | |_ \n");
   printf("| |   | | |/ _ \\ '_ \\| __|\n");
   printf("| |___| | |  __/ | | | |_ \n");
   printf("\\_____|_|_|\\___|_| |_|\\__|\n\n");

   //Check if the IP and port numbers are there
   if (argc < 3)
   {
      printf("Too few arguments!\n");
      return 1;
   }

   //Create socket
   socket_desc = socket(AF_INET, SOCK_STREAM, 0);
   if (socket_desc == -1)
   {
      printf("Could not create socket");
   }

   // server.sin_addr.s_addr = inet_addr("127.0.0.53");   
   server.sin_addr.s_addr = inet_addr(argv[1]);
   server.sin_family = AF_INET;
   // server.sin_port = htons(8888);
   server.sin_port = htons(atoi((char*)argv[2]));

   //Connect to remote server
   if (connect(socket_desc , (struct sockaddr *)&server , sizeof(server)) < 0)
   {
      puts("connect error");
      return 1;
   }
   else
   {
      recv(socket_desc, server_reply , 2000 , 0);
      printf("%s\n", server_reply);
   }

   while(1)
   {  
      char name[2048], port[2048], recr[2048];

      printf("Here are a list of commands:\n");
      printf("(1): Register someone\n");
      printf("(2): Assign port to person\n");
      printf("(3): List all the online users\n");
      printf("(4): Tranfer money\n");
      printf("(5): Exit\n");
      printf("Command: ");


      //Scanning the message and send the data
      scanf("%s", message);
      memset(server_reply, 0, sizeof(server_reply));
      send(socket_desc, message, strlen(message), 0); //send the command to the server

      if(strcmp(message, "1") == 0)
      {
         memset(name, 0, sizeof(name));
         printf("Name: ");
         scanf("%s", name);
         if (send(socket_desc, name, strlen(name), 0) < 0)
         {
            puts("Send failed");
            return -1;
         }
      }
      else if (strcmp(message, "2") == 0)
      {
         memset(name, 0, sizeof(name));
         memset(port, 0, sizeof(port));
         printf("Name: ");
         scanf("%s", name);
         if (send(socket_desc, name, strlen(name), 0) < 0)
         {
         	puts("Send failed");
            return -1;
         } 
         printf("Port: ");
         scanf("%s", port);
         if (send(socket_desc, port, strlen(port), 0) < 0)
         {
            puts("Send failed");
            return -1;
         }
      }
      else if (strcmp(message, "4") == 0)
      {
         memset(name, 0, sizeof(name));
         memset(recr, 0, sizeof(recr));
         memset(port, 0, sizeof(port));
         printf("Name: "); scanf("%s", name);
         if (send(socket_desc, name, strlen(name), 0) < 0)
         {
         	puts("Send failed");
            return -1;
         }
         printf("Receiver: "); scanf("%s", recr);
         if (send(socket_desc, recr, strlen(recr), 0) < 0)
         {
         	puts("Send failed");
            return -1;
         }
         printf("Amount: "); scanf("%s", port);
         if (send(socket_desc, port, strlen(port), 0) < 0)
         {
            puts("Send failed");
            return -1;
         }
      }
      else if (strcmp(message, "5") == 0)
      {
         recv(socket_desc, server_reply , 2000 , 0);
         puts(server_reply);
         break;
      }
      else if (strcmp(message, "3") != 0)
      {
         puts("Unsure command");
         goto clear_place;
      }
      
      int read_size = 0;
      FILE* fp = fdopen(socket_desc, "w");
      //Receive the reply from the server clear
      if ((read_size = recv(socket_desc, server_reply, 2000, 0)) > 0)
      {
         fflush(fp);
         printf("%s", server_reply);
         fflush(stdout);
         memset(server_reply, 0, sizeof(server_reply));
      }
      clear_place:
      memset(server_reply, 0, sizeof(server_reply));
      memset(message, 0, sizeof(message));
   }

   //Close the socket
   close(socket_desc);
   
   return 0;
}
