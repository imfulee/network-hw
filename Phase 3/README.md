# Network Homework Phase 3

- Compiler=gcc
- environment=Linux kernel >= 2.6
- Language=C
- The makefile is as follow:

```makefile
all: client.o server.o
	gcc -g client.o -o client -lssl -lcrypto
	gcc -g server.o -o server -lpthread -lssl -lcrypto
	make clean

client.o: client.c
	gcc -g -c client.c -lssl -lcrypto

server.o: server.c
	gcc -g -c server.c -lpthread -lssl -lcrypto

clean:
	rm *.o
```

- Compiling it into a binary would be to use the command `make`

## Usage of client binary

When using the client in terminal of a Linux environment

```bash
./client <IP address> <Port Number>
```

Then it would connect to the server, and the way to connect to the server would be to type in the commands, which is laid out on the command line.  

## Usage of server binary

When using the server in terminal of a Linux environment 

```bash
./server <Port number>
```

Then it would start the server with multiple threads!
