#include <stdio.h> //printf
#include <string.h> //strcpy
#include <stdlib.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>	//hostent
#include <arpa/inet.h>
#include <pthread.h>
#include <stdbool.h>
#include "handler.h"

int main(int argc, char *argv[])
{
	int socket_desc, new_socket, c, *new_sock;
	struct sockaddr_in server, client;
	char message[2048];

	printf(" ____  \n");
	printf("/ ___|  ___ _ ____   _____ _ __ \n");
	printf("\\___ \\ / _ \\ '__\\ \\ / / _ \\ '__|\n");
	printf(" ___) |  __/ |   \\ V /  __/ |   \n");
	printf("|____/ \\___|_|    \\_/ \\___|_|\n\n");

	/*
	if (argc < 2)
	{
		printf("Too few arguments. Hint: port_num?");
		return 1;
	}
	*/
	
	//Create socket
	socket_desc = socket(AF_INET, SOCK_STREAM, 0);
	if (socket_desc == -1)
	{
		printf("Could not create socket");
		return 1;
	}
	
	//Prepare the sockaddr_in structure
	server.sin_family = AF_INET;
	server.sin_addr.s_addr = INADDR_ANY;
	server.sin_port = htons(8888);
	// server.sin_port = htons(atoi((char*)argv[1]));
	
	//Bind
	if (bind(socket_desc,(struct sockaddr *)&server, sizeof(server)) < 0)
	{
		puts("bind failed");
		return 1;
	}
	
	//Listen
	listen(socket_desc, 5);
	
	//Accept and incoming connection
	puts("Waiting for incoming connections...");
	c = sizeof(struct sockaddr_in);
	while((new_socket = accept(socket_desc, (struct sockaddr *)&client, (socklen_t*)&c)))
	{
		puts("Connection accepted");
		
		pthread_t sniffer_thread;
		new_sock = malloc(1);
		*new_sock = new_socket;
		
		if(pthread_create(&sniffer_thread, NULL, connection_handler, (void*) new_sock) < 0)
		{
			perror("could not create thread");
			return 1;
		}
		
		//Now join the thread, so that we dont terminate before the thread
		//pthread_join( sniffer_thread, NULL);
		puts("Handler assigned");
	}
	
	if (new_socket<0)
	{
		perror("accept failed");
		return 1;
	}
		
	RSA_free(publicRsa);
	RSA_free(privateRsa);
	return 0;
}