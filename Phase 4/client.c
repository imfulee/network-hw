#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>   //strlen
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>   //inet_addr
#include "rsa.h"

int main(int argc , char *argv[])
{
   int socket_desc;
   struct sockaddr_in server;
   char message[2048], server_reply[2000];
   memset(server_reply, 0, sizeof(server_reply));
   
   if ((fp = fopen(PUBLICKEY, "r")) == NULL) 
   {
      printf("public key path error\n");
      return -1;
   }
   if ((publicRsa = PEM_read_RSA_PUBKEY(fp, NULL, NULL, NULL)) == NULL) 
   {
      printf("PEM_read_RSA_PUBKEY error\n");
      return -1;
   }
   fclose(fp);
   if ((fp = fopen(PRIVATEKEY, "r")) == NULL) 
   {
      printf("private key path error\n");
      return -1;
   }
   OpenSSL_add_all_algorithms();
   if ((privateRsa = PEM_read_RSAPrivateKey(fp, NULL, NULL, (char *)PASS)) == NULL) 
   {
      printf("PEM_read_RSAPrivateKey error\n");
      return 0;
   }
   fclose(fp);

   printf("  ____ _ _            _   \n");
   printf(" / ___| (_) ___ _ __ | |_ \n");
   printf("| |   | | |/ _ \\ '_ \\| __|\n");
   printf("| |___| | |  __/ | | | |_ \n");
   printf("\\_____|_|_|\\___|_| |_|\\__|\n\n");

   /*
   //Check if the IP and port numbers are there
   if (argc < 3)
   {
      printf("Too few arguments!\n");
      return 1;
   }
   */

   //Create socket
   socket_desc = socket(AF_INET, SOCK_STREAM, 0);
   if (socket_desc == -1)
   {
      printf("Could not create socket");
   }

   server.sin_addr.s_addr = inet_addr("127.0.0.53");   
   // server.sin_addr.s_addr = inet_addr(argv[1]);
   server.sin_family = AF_INET;
   server.sin_port = htons(8888);
   // server.sin_port = htons(atoi((char*)argv[2]));

   //Connect to remote server
   if (connect(socket_desc , (struct sockaddr *)&server , sizeof(server)) < 0)
   {
      puts("connect error");
      return 1;
   }
   else
   {
      recv(socket_desc, server_reply , 2000 , 0);
      printf("%s\n", server_reply);
   }

   while(1)
   {  
      int rsa_len = RSA_size(publicRsa);
      unsigned char *encryptMsg = (unsigned char *)malloc(rsa_len);
      memset(encryptMsg, 0, rsa_len);
      int len = rsa_len - 11;

      rsa_len = RSA_size(privateRsa);
      unsigned char *decryptMsg = (unsigned char *)malloc(rsa_len);
      memset(decryptMsg, 0, rsa_len);
      
      char name[2048], port[2048], recr[2048];

      printf("Here are a list of commands:\n");
      printf("(1): Register someone\n");
      printf("(2): Assign port to person\n");
      printf("(3): List all the online users\n");
      printf("(4): Tranfer money\n");
      printf("(5): Exit\n");
      printf("Command: ");


      //Scanning the message and send the data
      scanf("%s", message);
      memset(server_reply, 0, sizeof(server_reply));
      RSA_public_encrypt(len, message, encryptMsg, publicRsa, RSA_PKCS1_PADDING);
      send(socket_desc, encryptMsg, strlen(encryptMsg), 0); //send the command to the server
      memset(encryptMsg, 0, sizeof(encryptMsg));

      if(strcmp(message, "1") == 0)
      {
         memset(name, 0, sizeof(name));
         printf("Name: "); scanf("%s", name);
         RSA_public_encrypt(len, name, encryptMsg, publicRsa, RSA_PKCS1_PADDING);
         if (send(socket_desc, encryptMsg, strlen(encryptMsg), 0) < 0)
         {
            puts("Send failed");
            return -1;
         }
         memset(encryptMsg, 0, sizeof(encryptMsg));
      }
      else if (strcmp(message, "2") == 0)
      {
         memset(name, 0, sizeof(name));
         memset(port, 0, sizeof(port));
         
         printf("Name: "); scanf("%s", name);
         RSA_public_encrypt(len, name, encryptMsg, publicRsa, RSA_PKCS1_PADDING);
         if (send(socket_desc, encryptMsg, strlen(encryptMsg), 0) < 0)
         {
         	puts("Send failed");
            return -1;
         } 
         memset(encryptMsg, 0, sizeof(encryptMsg));
         
         printf("Port: "); scanf("%s", port);
         RSA_public_encrypt(len, port, encryptMsg, publicRsa, RSA_PKCS1_PADDING);
         if (send(socket_desc, port, strlen(port), 0) < 0)
         {
            puts("Send failed");
            return -1;
         }
         memset(encryptMsg, 0, sizeof(encryptMsg));
      }
      else if (strcmp(message, "4") == 0)
      {
         memset(name, 0, sizeof(name));
         memset(recr, 0, sizeof(recr));
         memset(port, 0, sizeof(port));

         printf("Name: "); scanf("%s", name);
         RSA_public_encrypt(len, name, encryptMsg, publicRsa, RSA_PKCS1_PADDING);
         if (send(socket_desc, encryptMsg, strlen(encryptMsg), 0) < 0)
         {
            puts("Send failed");
            return -1;
         } 
         memset(encryptMsg, 0, sizeof(encryptMsg));

         printf("Receiver: "); scanf("%s", recr);
         RSA_public_encrypt(len, recr, encryptMsg, publicRsa, RSA_PKCS1_PADDING);
         if (send(socket_desc, encryptMsg, strlen(encryptMsg), 0) < 0)
         {
            puts("Send failed");
            return -1;
         } 
         memset(encryptMsg, 0, sizeof(encryptMsg));

         printf("Amount: "); scanf("%s", port);
         RSA_public_encrypt(len, port, encryptMsg, publicRsa, RSA_PKCS1_PADDING);
         if (send(socket_desc, encryptMsg, strlen(encryptMsg), 0) < 0)
         {
            puts("Send failed");
            return -1;
         } 
         memset(encryptMsg, 0, sizeof(encryptMsg));
      }
      else if (strcmp(message, "5") == 0)
      {
         recv(socket_desc, server_reply , 2000 , 0);
         RSA_private_decrypt(rsa_len, server_reply, decryptMsg, privateRsa, RSA_PKCS1_PADDING);
         puts(server_reply);
         memset(decryptMsg, 0, sizeof(decryptMsg));
         break;
      }
      else if (strcmp(message, "3") != 0)
      {
         puts("Unsure command");
         goto clear_place;
      }
      
      int read_size = 0;
      FILE* fp = fdopen(socket_desc, "w");
      //Receive the reply from the server clear
      if ((read_size = recv(socket_desc, server_reply, 2000, 0)) > 0)
      {
         fflush(fp);
         RSA_private_decrypt(rsa_len, server_reply, decryptMsg, privateRsa, RSA_PKCS1_PADDING);
         printf("%s", server_reply);
         fflush(stdout);
         memset(server_reply, 0, sizeof(server_reply));
         memset(decryptMsg, 0, sizeof(decryptMsg));
      }
      clear_place:
      memset(server_reply, 0, sizeof(server_reply));
      memset(message, 0, sizeof(message));
      memset(decryptMsg, 0, sizeof(decryptMsg));
      memset(encryptMsg, 0, sizeof(encryptMsg));
   }

   //Close the socket
   close(socket_desc);
      
   RSA_free(publicRsa);
   RSA_free(privateRsa);
   return 0;
}