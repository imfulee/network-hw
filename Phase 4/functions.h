#include <stdlib.h>
#include <string.h>
#define MAXUSERNUM 20

struct People
{
	char port_num[2048];
	char ip_number[2048];
	char name[2048];
	int balance;
	bool online;
};

struct People user[MAXUSERNUM];
int total_user = 0, online_user = 0;

bool name_exist(char name[])
{
	if (total_user == 0) return false;
	for (int i = 0 ; i < total_user ; i++)
	{
		if (strcmp(name, user[i].name) == 0)
		{
			return true;
		}
	}
	return false;
}

int find_user(char name[])
{
	for (int i = 0 ; i < total_user ; i++)
	{
		if (strcmp(name, user[i].name) == 0)
		{
			return i;
		}
	}
	return -1;
}

bool cat_alluser(char* message)
{
	if (total_user == 0 || online_user == 0) return false;
	char header[2000];
	sprintf(header, "Account Balance\n# of accounts = %d\n", total_user);
	strcat(message, header);
	for(int i = 0 ; i < total_user ; i++)
	{
		char buffer[8000];
		if (user[i].online == true)
		{
			sprintf(buffer, "%s#%s#%s\n", user[i].name, user[i].ip_number, user[i].port_num);
			strcat(message, buffer);
		}
	}
	return true;
}

bool create_user(char name[])
{
	if (name_exist(name)) return false;
	strcpy(user[total_user].name, name);
	user[total_user].balance = 1000;
	user[total_user].online = false;
	strcpy(user[total_user].ip_number, "127.0.0.53");
	// Hopefully next time I could get the real IP 
	total_user += 1;
	return true;
}

bool transfer(char from[], char to[], int amount)
{
	if (name_exist(from) && name_exist(to))
	{
		if (user[find_user(from)].balance < amount ||
			!user[find_user(from)].online ||
			!user[find_user(to)].online)
		{
			return false;
		}
		
		user[find_user(from)].balance -= amount;
		user[find_user(to)].balance += amount;
		return true;
	}
	return false;
}

bool assign_num(char name[], char port[])
{
	if (!name_exist(name)) return false;
	strcpy(user[find_user(name)].port_num, port);
	user[find_user(name)].online = true;
	online_user += 1;
	return true;
}