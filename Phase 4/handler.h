#include "functions.h"
#include "rsa.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>

void *connection_handler(void *socket_desc)
{
	if ((fp = fopen(PUBLICKEY, "r")) == NULL) 
   {
      printf("public key path error\n");
   }
   if ((publicRsa = PEM_read_RSA_PUBKEY(fp, NULL, NULL, NULL)) == NULL) 
   {
      printf("PEM_read_RSA_PUBKEY error\n");
   }
   fclose(fp);
   if ((fp = fopen(PRIVATEKEY, "r")) == NULL) 
   {
      printf("private key path error\n");
   }
   OpenSSL_add_all_algorithms();
   if ((privateRsa = PEM_read_RSAPrivateKey(fp, NULL, NULL, (char *)PASS)) == NULL) 
   {
      printf("PEM_read_RSAPrivateKey error\n");
   }
   fclose(fp);

   int rsa_len = RSA_size(publicRsa);
   unsigned char *encryptMsg = (unsigned char *)malloc(rsa_len);
   memset(encryptMsg, 0, rsa_len);
   int len = rsa_len - 11;

   rsa_len = RSA_size(privateRsa);
   unsigned char *decryptMsg = (unsigned char *)malloc(rsa_len);
   memset(decryptMsg, 0, rsa_len);

	//Get the socket descriptor
	int sock = *(int*)socket_desc;
	int read_size = 0;
	char message[100000], client_message[2000], name[2048], port[2048], recr[2048];
	strcpy(message, "Connection accepted\r\n");
	RSA_public_encrypt(len, message, encryptMsg, publicRsa, RSA_PKCS1_PADDING);
	write(sock, encryptMsg, strlen(encryptMsg));
	memset(message, 0, sizeof(message));
	memset(encryptMsg, 0, sizeof(encryptMsg));

	//Receive a message from client
	while(read_size = recv(sock, client_message, sizeof(client_message), 0) > 0)
	{
		memset(message, 0, sizeof(message));
		memset(name, 0, sizeof(name));
		memset(port, 0, sizeof(port));
		memset(recr, 0, sizeof(recr));
		RSA_private_decrypt(rsa_len, client_message, decryptMsg, privateRsa, RSA_PKCS1_PADDING);

		if (strcmp(decryptMsg, "1") == 0)
		{
			memset(decryptMsg, 0, sizeof(decryptMsg));
			read_size = recv(sock, client_message, 2000, 0);
			RSA_private_decrypt(rsa_len, client_message, decryptMsg, privateRsa, RSA_PKCS1_PADDING);
			printf("REGISTER#%s\n", decryptMsg);
			memset(decryptMsg, 0, sizeof(decryptMsg));
			
			if (name_exist(decryptMsg))
			{
				strcpy(message, "210 FAIL\r\n");
			}
			else
			{
				strcpy(message, "100 OK\r\n");
				create_user(decryptMsg);
			}
			
			RSA_public_encrypt(len, message, encryptMsg, publicRsa, RSA_PKCS1_PADDING);
			write(sock, message, strlen(message));	
			memset(encryptMsg, 0, sizeof(encryptMsg));
			memset(message, 0, sizeof(message));
		}
		else if (strcmp(decryptMsg, "2") == 0)
		{
			char buffer[5000]; memset(buffer, 0, sizeof(buffer));
			memset(decryptMsg, 0, sizeof(decryptMsg));

			read_size = recv(sock, name, sizeof(name), 0);
			RSA_private_decrypt(rsa_len, name, decryptMsg, privateRsa, RSA_PKCS1_PADDING);
			memset(name, 0, sizeof(name));
			strcpy(name, decryptMsg);
			memset(decryptMsg, 0, sizeof(decryptMsg));

			read_size = recv(sock, port, sizeof(port), 0);
			RSA_private_decrypt(rsa_len, port, decryptMsg, privateRsa, RSA_PKCS1_PADDING);
			memset(port, 0, sizeof(port));
			strcpy(port, decryptMsg);
			memset(decryptMsg, 0, sizeof(decryptMsg));

			sprintf(buffer, "%s#%s\n", name, port);
			printf("%s", buffer); // to comply with my teacher
			if (name_exist(name))
			{
				assign_num(name, port);
				cat_alluser(message);
			}
			else
			{
				strcpy(message, "220 AUTH FAIL\r\n");
			}

			RSA_public_encrypt(len, message, encryptMsg, publicRsa, RSA_PKCS1_PADDING);
			write(sock, message, strlen(message));
			memset(encryptMsg, 0, sizeof(encryptMsg));
			memset(message, 0, sizeof(message));
		}
		else if (strcmp(decryptMsg, "3") == 0)
		{
			memset(decryptMsg, 0, sizeof(decryptMsg));
			puts("List");
			if (!cat_alluser(message))
			{
				strcpy(message, "220 AUTH FAIL\r\n");
			}
			
			RSA_public_encrypt(len, message, encryptMsg, publicRsa, RSA_PKCS1_PADDING);
			write(sock, message, strlen(message));
			memset(encryptMsg, 0, sizeof(encryptMsg));
			memset(message, 0, sizeof(message));
		}
		else if (strcmp(decryptMsg, "4") == 0)
		{
			memset(decryptMsg, 0, sizeof(decryptMsg));
			
			read_size = recv(sock, name, sizeof(name), 0);
			RSA_private_decrypt(rsa_len, name, decryptMsg, privateRsa, RSA_PKCS1_PADDING);
			memset(name, 0, sizeof(name));
			strcpy(name, decryptMsg);
			memset(decryptMsg, 0, sizeof(decryptMsg));
			
			read_size = recv(sock, recr, sizeof(recr), 0);
			RSA_private_decrypt(rsa_len, recr, decryptMsg, privateRsa, RSA_PKCS1_PADDING);
			memset(recr, 0, sizeof(recr));
			strcpy(recr, decryptMsg);
			memset(decryptMsg, 0, sizeof(decryptMsg));
			
			read_size = recv(sock, port, sizeof(port), 0);
			RSA_private_decrypt(rsa_len, port, decryptMsg, privateRsa, RSA_PKCS1_PADDING);
			memset(port, 0, sizeof(port));
			strcpy(port, decryptMsg);
			memset(decryptMsg, 0, sizeof(decryptMsg));
			
			printf("%s#%s#%s\n", name, port, recr);

			if (name_exist(name) && name_exist(recr))
			{
				if (transfer(name, recr, atoi(port))) strcpy(message, "Transfer accepted\r\n");
				else strcpy(message, "Not enough funds\r\n");
			}
			else strcpy(message, "User not found\r\n");
			
			RSA_public_encrypt(len, message, encryptMsg, publicRsa, RSA_PKCS1_PADDING);
			write(sock, encryptMsg, strlen(encryptMsg));
			memset(encryptMsg, 0, sizeof(encryptMsg));
			memset(message, 0, sizeof(message));
		}
		else if (strcmp(decryptMsg, "5") == 0)
		{
			memset(decryptMsg, 0, sizeof(decryptMsg));
			puts("Exit");
			online_user -= 1;
			strcpy(message, "Bye\r\n");
			write(sock, message, strlen(message));
			puts("Client disconnected");
			close(sock);
			break;
		}
		else
		{
			memset(decryptMsg, 0, sizeof(decryptMsg));
			strcpy(message, "Understand your message, server doesn't\n");
			write(sock, message, strlen(message));
			memset(message, 0, sizeof(message));
			continue;
		}
		memset(client_message, 0 , sizeof(client_message));
		memset(decryptMsg, 0, sizeof(decryptMsg));
		memset(encryptMsg, 0, sizeof(encryptMsg));
	}
	
	if(read_size == 0)
	{
		puts("Client disconnected");
		fflush(stdout);
	}
	else if(read_size == -1)
	{
		perror("recv failed");
	}
		
	//Free the socket pointer
	free(socket_desc);
	return 0;
}