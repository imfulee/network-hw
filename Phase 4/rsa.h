#include <openssl/pem.h>
#include <openssl/ssl.h>
#include <openssl/rsa.h>
#include <openssl/evp.h>
#include <openssl/bio.h>
#include <openssl/err.h>
#include <openssl/x509.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <stdbool.h>

#define PUBLICKEY "publicKey.pem"
#define PRIVATEKEY "privateKey.pem"
#define PASS 8888

FILE *fp = NULL;
RSA *publicRsa = NULL;
RSA *privateRsa = NULL;